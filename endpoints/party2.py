from .database import playerDB, accesstokenDB
import json

def party2(request):
    pid = accesstokenDB[request.headers['Authorization']]['id']
    parties=json.loads(request.data)['param']['parties']
    playerDB.edit({'id':pid},{'parties':parties})
    return {'parties':parties}