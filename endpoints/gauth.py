from .database import accesstokenDB, playerDB
from .main import UNIX_timestamp
from .playnew import playnew
import uuid
import json
####    gauth/register
def gauth_register(request):
    #generate device_id
    keys=json.loads(request.data)['param']
    did=str(uuid.uuid4())
    sid=keys['secret_key']
    while playerDB.find({'device_id':did,'secret_key':sid}):
        did=str(uuid.uuid4())
    
    #create new account here to skip tutorial
    pid=playerDB.insert({'device_id':did,'secret_key':sid})
    #accound created -> access token
    accesstoken=str(uuid.uuid4()).replace('-','')[:30]
    while accesstoken in accesstokenDB:
        accesstoken=str(uuid.uuid4()).replace('-','')[:30]
    accesstokenDB['gauth %s'%accesstoken]={
        'id':   pid,
        'creation': UNIX_timestamp(),
        **keys
    }
    # access token created -> generate account infos
    #request.headers['Authorization']='gauth %s'%accesstoken
    playnew(request,{'device_id':did,'secret_key':sid,'id':pid})

    #return normal register
    return {'device_id':did}
'''
POST /gauth/register HTTP/1.1
{"ticket":"0",
"access_token":"",
"param":{
    "udid":"",
    "secret_key":"d9be4da3-8688-4780-b60f-30031c9e7c34",
    "idfv":"ad61b5b1-9bd6-381b-ac64-3ba9e22e583f",
    "idfa":"0aaa21c2-7b55-4818-b1b8-55abb1621fb2"}
    }

ret:
"body": {
		"device_id": "587a1e93-db70-4632-8c3b-4343058c2011"
	}
'''

####    gauth accesstoken
def gauth_accesstoken(request):
    keys=json.loads(request.data)['param']
    did=keys['device_id']
    sid=keys['secret_key']
    accesstoken=str(uuid.uuid4()).replace('-','')[:30]
    while accesstoken in accesstokenDB:
        accesstoken=str(uuid.uuid4()).replace('-','')[:30]

    player=playerDB.find({'device_id':did,'secret_key':sid})
    if player:
        accesstokenDB['gauth %s'%accesstoken]={
            'id':   player['id'],
            'creation': UNIX_timestamp(),
            **keys
        }
        return {'access_token':accesstoken, 'expires_in': 86400}
    else:#   No Existing Account - creating one
        pid=playerDB.insert({'device_id':did,'secret_key':sid})

        accesstokenDB['gauth %s'%accesstoken]={
            'id':   pid,
            'creation': UNIX_timestamp(),
            **keys
        }
        # access token created -> generate account infos
        #request.headers['Authorization']='gauth %s'%accesstoken
        playnew(request,{'device_id':did,'secret_key':sid,'id':pid})
        # account finished -> return access token
        return {'access_token':accesstoken, 'expires_in': 86400}
'''
{
	"ticket": "0",
	"access_token": "",
	"param": {
		"secret_key": "d9be4da3-8688-4780-b60f-30031c9e7c34",
		"device_id": "587a1e93-db70-4632-8c3b-4343058c2011",
		"idfv": "ad61b5b1-9bd6-381b-ac64-3ba9e22e583f",
		"idfa": "0aaa21c2-7b55-4818-b1b8-55abb1621fb2",
		"udid": ""
	}
}

ret:
	"body": {
		"access_token": "VpW9oPBuXozENUIeHpT6Ovff0POD1L",
		"expires_in": 86400
	}
'''