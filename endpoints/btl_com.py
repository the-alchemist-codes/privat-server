from .main import UNIX_timestamp, requestFromServer, json, API, randomKey, questparam
from .database import playerDB, accesstokenDB
import random

def btl_com(request):
	#body,headers = requestFromServer(API,request)
	#body=json.loads(body)

	startTime=UNIX_timestamp()-60*60*24
	endTime=UNIX_timestamp()+60*60*24

	def boundary(iname):
		ret={
			"i": iname,
			"n": 0,
			"t": 0,
			"m": 0,
			# "d": {
			# 	"num": 1,
			# 	"reset": 0
			# }
		}
		if iname[3:5] != 'ST':
			ret.update({
				"s": startTime,
				"e": endTime,
			})
		return ret
	body=[
			boundary(iname)
			for iname in questparam['quests']
			if iname[5]=='_'
	]
	return body

def btl_com_req(request):
	#body,headers = requestFromServer(API,request)
	#body=json.loads(body)
	loginKeys= accesstokenDB[request.headers['Authorization']]
	pid=loginKeys['id']
	# if body['stat'] != 0:   #any error -> send valid response
	# 	#remove error part
	# 	body.update({
	# 		"stat": 0,
	# 		"stat_msg": "",
	# 		"stat_code": "",
	# 	})
	# 	#create body
	if True:
		req_body = json.loads(request.data)
		body={#body['body']= {
			"btlid": random.randint(10000000,99999999),
			"btlinfo": {
				"qid": req_body['param']['iname'],
				"units": [{'iid':uid} for uid in playerDB.find({'id':pid})['parties'][req_body['param']['partyid']]['units']],
				# [{
				# 	"iid": 153134440
				# }, {
				# 	"iid": 152060627
				# }, {
				# 	"iid": 150782081
				# }, {
				# 	"iid": 144936844
				# }, {
				# 	"iid": 151160700
				# }, {
				# 	"iid": 153146509
				# }],
				"help": {},
				"drops": [],
				"key": randomKey(16),
				"seed": random.randint(0000,9999),
				"campaigns": [],
				"start_at": UNIX_timestamp(),
				"lot_enemies": [],
				"stamina": {
					"use": 0,
					"base": 0
				}
			}
		}
	else:
		pass

	return body

def btl_com_end(request):
	#body,headers = requestFromServer(API,request)
	#body=json.loads(body)
	# if body['stat'] != 0:   #any error -> send valid response
	# 	#remove error part
	# 	body.update({
	# 		"stat": 0,
	# 		"stat_msg": "",
	# 		"stat_code": "",
	# 	})
	# else:
	# 	pass

	return {}