import datetime
import os
import json
import http.client
import random
import flask

#settings
API='app.alcww.gumi.sg' #"alchemist.gu3.jp"
host_ap_o='https://alchemist.gu3.jp'
host_dl= "http://prod-dlc-alcww-gumi-sg.akamaized.net/"#"https://alchemist-dlc2.gu3.jp"#'http://127.0.0.2/'
host_ap= "http://tacps.serveo.net/" #'http://127.0.0.1/'#"https://alchemist.gu3.jp" #'localhost:5000' switches to http

PATH = os.path.join(os.path.realpath(__file__),*[os.pardir,os.pardir])

#masterJP = json.loads(open(os.path.join(PATH,*['resources','MasterParamJP.json']),'rb').read())
masterparam= {main:{item['iname']:item for item in tree if type(tree[0])==dict and 'iname' in tree[0]} for main,tree in json.loads(open(os.path.join(PATH,*['resources','MasterParam.json']),'rb').read()).items() if type(tree)==list}
questparam= {main:{item['iname']:item for item in tree if type(tree[0])==dict and 'iname' in tree[0]} for main,tree in json.loads(open(os.path.join(PATH,*['resources','QuestParam.json']),'rb').read()).items() if type(tree)==list}

#functions
def readJson( file ):
	with open( file ) as f:
		data = json.load(f)
	return data	

def randomKey(length):
			#A-Z							a-z								0-10
	keys=[*[chr(i) for i in range(65,91)],*[chr(i) for i in range(97,123)],*[str(i) for i in range(10)],'+','-','_']
	return ''.join([
		random.choice(keys)
		for i in range(length)
	])

def answer(body):
	if 'stat' not in body:
		body={
			"stat": 0,
			"stat_msg": "",
			"stat_code": "",
			"time": UNIX_timestamp(),
			"body": body
		}
	return flask.jsonify(body)

def requestFromServer(host,req):
	#pre fix
	host=host #req.host
	path=req.path
	body=req.data.decode('utf8')
	headers=dict(req.headers)

	#Fix
	headers['Host']=host
	if 'Content-Length' in headers:
		del headers['Content-Length']
	method=req.method

	#request
	print(method,host,path,'\n',body,'\n',headers)
	try:
		con = http.client.HTTPSConnection(host=host)
		con.connect()
		con.request(method , path, body, headers)
		#get response
		con_resp = con.getresponse()
		body =		con_resp.read().decode('utf8')
		headers =	dict(con_resp.headers)
		#close connection
		con.close()
	except Exception as e:
		print(e)
		body={}
		headers={}


	return (body,headers)

def responseHeaders():
	return {
		"Date": 		'{:%a, %d %b %Y %H:%M:%S} GMT'.format(datetime.datetime.utcnow()),#"Tue, 01 Jan 2019 22:37:31 GMT",
		"Content-Type": "application/json",
		"Server": 		"nginx/1.9.3",
		"X-Powered-By": "Express",
		"Vary": 		"X-HTTP-Method-Override",
		"Transfer-Encoding": "chunked",
		"Connection": "Keep-alive",
		}

####	timestamps	######################################
def ymd_timestamp(api='app.alcww.gumi.sg',date=False,deltaH=8):
	if 'app.alcww.gumi.sg' in api:
		deltaH=8
	elif 'alchemist.gu3.jp' in api:
		deltaH=14

	if not date:
		return '{:%y%m%d}'.format(datetime.datetime.utcnow() - datetime.timedelta(hours=deltaH))
	else:
		return '{:%y%m%d}'.format(datetime.date(*date) - datetime.timedelta(hours=deltaH))

def UNIX_timestamp(date=False):
	if not date:
		return round((datetime.datetime.utcnow() - datetime.datetime(1970,1,1)).total_seconds())
	else:
		return round((datetime.date(*date) - datetime.datetime(1970,1,1)).total_seconds())