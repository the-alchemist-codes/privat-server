# The Alchemist Code - Privat Server

## Requirements:
* python 3.6
* flask

## Progress:
So far it redirects most rquests to the original server and edits some of the responses,
e.g. so that one can run all ever avaible quests.

The framework is done,
just the database as well as the implementation of all possible requests is missing.

## How To Play:
1. Install mod_apk\TAC_mod.apk
2. Run flask_api.py
3. Run serveo.bat
3. Start apk

## APK-Editing:
### requirements:
* Visual Studio (ildasm and ilasm)
* Java Runtime Enviroment

### notes:
* batch files are designed for drag&drop or ???.bat target
* you may have to edit the paths of some of the batch files

### guideline:
1. Download the latest APK
2. Extract Assets\bin\Data\Managed\Assembly-CSharp.dll and throw it at "__DLL_to_IL.bat"
3. Open the .il with any editor and change the urls.
    (https://app.alcww.gumi.sg/ to http://tacps.serveo.net/ or whatever you are using)
4. Compile the edited .il back into a .dll by using "__IL_to_DLL.bat"
5. Put the edited .dll back into the apk
6. Delete the META_INF folder in the APK
7. Use "_Sign_APK.bat" to sign the apk (so that it can be installed without root and exposed)

Build and Decode APK can be used to edit the app container name as well as where the data can be safed,
I wasn' able to do so successfully so far tho (batchs are working).

## Sources
* APK-Signing: https://blogs.sap.com/2014/05/21/how-to-modify-an-apk-file/
* APK editing with apktool: https://forum.xda-developers.com/showthread.php?t=2760965