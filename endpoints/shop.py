import json

#placeholders

def shop(request):
    typ=json.loads(request.data)['param']['iname']
    #   Normal, AwakePiece
    return shopitems

def shop_buy(request):
    return buy

def shop_event(request):
    return shopitems

def shop_event_buy(request):
    return buy

def shop_limited(request):
    return shopitems

def shop_limited_buy(request):
    return buy

def shop_event_shoplist(request):
    return {'shops':[]}
    
def shop_limited_shoplist(request):
    return {'shops':[]}

shopitems={"shopitems": [{
			"id": 0,
			"sold": 0,
			"item": {
				"iname": "IT_UG_APPLE2",
				"num": 10
			},
			"cost": {
				"type": "gold",
				"value": 0
			}}]}

buy={"currencies": {
			"gold": 1000000,
			"coin": {
				"free": 0,
				"paid": 0,
				"com": 0
			},
			"arenacoin": 0,
			"multicoin": 0,
			"enseicoin": 0,
			"kakeracoin": 0,
		},
		"shopitems": [],
		"items": []
	}