import endpoints
from time import sleep
import flask
import json


# SETTINGS
API='app.alcww.gumi.sg'#'alchemist.gu3.jp'
API_PORT=443
CONNECTION_TYPE='HTTP'
PRINT_REQUESTS=False

# ON/OFF
CHKVER2=False

# FLASK API
app = flask.Flask(__name__)

@app.before_request
def my_method():
	# get the /sleep param
	# pass the ?sleep=x url param where x is time is seconds
	req=flask.request
	print('Request:',req.method,req.base_url,)
	if PRINT_REQUESTS:
		print('Headers')
		print(json.dumps(dict(flask.request.headers),indent='\t'))

		print('Data')
		data=flask.request.data
		try:
			data=json.loads(flask.request.data)
		except:
			pass
		if type(data)==bytes:
			data=str(data)
		print(json.dumps(data,indent='\t'))

		print('Arguments')
		print(json.dumps(dict(flask.request.args),indent='\t'))

@app.after_request
def after_req(resp):
	if resp.data in [None, b'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">\n<title>404 Not Found</title>\n<h1>Not Found</h1>\n<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>\n', b'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">\n<title>404 Not Found</title>\n<h1>Not Found</h1>\n<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>\n']:
		#body,headers = endpoints.requestFromServer(API,flask.request)
		body=endpoints.answer({}).data
	else:
		body=resp.data
		#headers=endpoints.responseHeaders()		

	print(json.dumps(json.loads(body),indent='\t', ensure_ascii=False))
	#generate response for the flask api request
	resp=flask.Response(body,status=200, mimetype='application/json')#,headers=headers)
	return resp


# endpoints ------------------------------------------------------------------------------------------------------------------------------------------

@app.route("/")
def info():
	return

####	boot-up
@app.route('/chkver', methods=['GET', 'POST'])
@app.route('/chkver2', methods=['GET', 'POST'])
def chkver2():
	return endpoints.answer(endpoints.chkver2(flask.request))

@app.route('/player/chkplayer', methods=['GET','POST'])
def chkplayer():
	return endpoints.answer({"result": 1})

@app.route('/bundle', methods=['GET','POST'])
def bundle():
	return endpoints.answer({"bundles": []})

@app.route('/product', methods=['GET','POST'])
def product():
	return endpoints.answer({"products": []})

@app.route('/achieve/auth', methods=['GET','POST'])
def achieve_auth():
	return endpoints.answer({"auth_status": 2})

#### new account
@app.route('/master/log', methods=['GET','POST'])
def master_log():
	return endpoints.answer({})

@app.route('/setlanguage', methods=['GET','POST'])
def setlanguage():
	return endpoints.answer(endpoints.login(flask.request))

@app.route('/tut/update', methods=['GET','POST'])
def tut_update():
	return endpoints.answer({})

#### user database related
@app.route('/gauth/accesstoken', methods=['GET','POST'])
def gauth_accesstoken():
	return endpoints.answer(endpoints.gauth_accesstoken(flask.request))

@app.route('/gauth/register', methods=['GET','POST'])
def gauth_register():
	return endpoints.answer(endpoints.gauth_register(flask.request))

@app.route('/playnew', methods=['GET','POST'])
def playnew():
	return endpoints.answer(endpoints.playnew(flask.request))

@app.route('/login', methods=['GET','POST'])
def login():
	return endpoints.answer(endpoints.login(flask.request))

@app.route('/login/param', methods=['GET','POST'])
def login_param():
	return endpoints.answer(endpoints.login_param(flask.request))

#### chat	~	if it might come, then as last thing
@app.route('/chat/channel', methods=['GET','POST'])
def chat_channel():
	return endpoints.answer({"channels": [{"id": 1,	"fever_level": 0}]})

@app.route('/chat/message', methods=['GET','POST'])
def chat_message():
	return endpoints.answer({"messages":[]})

@app.route('/chat/send', methods=['GET','POST'])
def chat_send():
	return endpoints.answer({"is_success": 1})


#### various stuff
@app.route('/home', methods=['GET','POST'])
def home():
	return endpoints.answer({"player": {"areamail_enabled": 0,"present_granted": 0}})

@app.route('/bingo/exec', methods=['GET','POST'])
def bingo_exec():
	return endpoints.answer({})

@app.route('/trophy/exec', methods=['GET','POST'])
def trophy_exec():
	return endpoints.answer({})

@app.route('/vs/rankmatch/reward', methods=['GET','POST'])
def vs_rankmatch_reward():
	body={"score": 0,"rank": 0,"type": 0,"schedule_id": 0,"reward": {"ranking": "",	"type": ""},"player": {}}
	return endpoints.answer(body)

#### battle	-	just bypassing restrictions atm
# /btl/com/resume
@app.route('/btl/com', methods=['GET', 'POST'])
def btl_com():
	return endpoints.answer(endpoints.btl_com(flask.request))

@app.route('/btl/com/req', methods=['GET','POST'])
def btl_com_req():
	return endpoints.answer(endpoints.btl_com_req(flask.request))
	
@app.route('/btl/com/end', methods=['GET','POST'])
def btl_com_end():
	return endpoints.answer(endpoints.btl_com_end(flask.request))

@app.route('/party2', methods=['GET','POST'])
def party2():
	return endpoints.answer(endpoints.party2(flask.request))

####	mail
@app.route('/mail', methods=['GET','POST'])
def mail():
	return endpoints.answer(endpoints.mail(flask.request))

@app.route('/mail/read', methods=['GET','POST'])
def mail_read():
	return endpoints.answer(endpoints.mail_read(flask.request))

####	gacha
@app.route('/gacha', methods=['GET','POST'])
def gacha():
	return endpoints.answer(endpoints.gacha(flask.request))

@app.route('/gacha/exec', methods=['GET','POST'])
def gacha_exec():
	return endpoints.answer(endpoints.gacha_exec(flask.request))

####	shops
@app.route('/shop', methods=['GET','POST'])
def shop():
	return endpoints.answer(endpoints.shop(flask.request))

@app.route('/shop/buy', methods=['GET','POST'])
def shop_buy():
	return endpoints.answer(endpoints.shop_buy(flask.request))

@app.route('/shop/event', methods=['GET','POST'])
def shop_event():
	return endpoints.answer(endpoints.shop_event(flask.request))

@app.route('/shop/event/buy', methods=['GET','POST'])
def shop_event_buy():
	return endpoints.answer(endpoints.shop_event_buy(flask.request))

@app.route('/shop/event/shoplist', methods=['GET','POST'])
def shop_event_shoplist():
	return endpoints.answer(endpoints.shop_event_shoplist(flask.request))

@app.route('/shop/limited', methods=['GET','POST'])
def shop_limited():
	return endpoints.answer(endpoints.shop_limited(flask.request))

@app.route('/shop/limited/buy', methods=['GET','POST'])
def shop_limited_buy():
	return endpoints.answer(endpoints.shop_limited_buy(flask.request))

@app.route('/shop/limited/shoplist', methods=['GET','POST'])
def shop_limited_shoplist():
	return endpoints.answer(endpoints.shop_limited_shoplist(flask.request))

#main	###############################################
if __name__=='__main__':
	app.run(port=80)


'''
@app.route('/', methods=['GET','POST'])
def ():
	return endpoints.answer()
'''