import json

def gacha(request):
    return {'gachas':gachas}

def gacha_exec(request):
    return {	
        "items": [{
			"iid": 51916402,
			"iname": "IT_US_SUMMONS_GL02",
			"num": 10
		}],
		"units": [],
		"artifacts": [],
		"add": [{
			"iname": "IT_US_SUMMONS_GL02",
			"num": 10,
			"iname_origin": "",
			"type": "item",
			"is_new": 1
		}],
		"add_mail": [],
		"receipt": {
			"iname": json.loads(request.data)['param']['gachaid'],
			"type": "gold",
			"val": 0
		}
	}

gachas= [{
			"iname": "Normal_Gacha",
			"cat": "gold_1",
			"startat": 1496286300,
			"endat": 4085957100,
			"num": 1,
			"name": "Normal Summon",
			"cost": {
				"gold": 10000
			},
			"asset_title": "title_normal_summon",
			"asset_bg": "GachaImages_new_normal",
			"detail_url": "normal_gacha",
			"units": [],
			"ext_type": [],
			"ext_param": {},
			"is_valid": 0,
			"group": "",
			"bonus_items": [],
			"pickup_art": []
		}, {
			"iname": "Normal_Gacha_10",
			"cat": "gold_n",
			"startat": 1499785200,
			"endat": 4094294340,
			"num": 10,
			"name": "Normal Summon",
			"cost": {
				"gold": 90000
			},
			"asset_title": "title_normal_summon",
			"asset_bg": "GachaImages_new_normal",
			"detail_url": "normal_gacha",
			"units": [],
			"ext_type": [],
			"ext_param": {},
			"is_valid": 0,
			"group": "",
			"bonus_items": [],
			"pickup_art": []
		}]