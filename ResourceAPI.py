from time import sleep
import flask
import json
import http.client

# SETTINGS
API='prod-dlc-alcww-gumi-sg.akamaized.net'#'alchemist.gu3.jp'
API_PORT=80
CONNECTION_TYPE='HTTP'
PRINT_REQUESTS=False

# ON/OFF
CHKVER2=False

# FLASK API
app = flask.Flask(__name__)

@app.before_request
def my_method():
	# get the /sleep param
	# pass the ?sleep=x url param where x is time is seconds
	req=flask.request
	print('Request:',req.method,req.base_url,)

@app.after_request
def after_req(resp):
	body,headers = requestFromServer(API,flask.request)
	#generate response for the flask api request
	resp=flask.Response(body,status=200)#,headers=headers)
	return resp


# endpoints ------------------------------------------------------------------------------------------------------------------------------------------

@app.route("/")
def info():
	return

def requestFromServer(host,req):
	#pre fix
	host=host #req.host
	path=req.path
	headers=dict(req.headers)

	#Fix
	headers['Host']=host
	method=req.method

	#request
	try:
		con = http.client.HTTPSConnection(host=host)
		con.connect()
		con.request(method , path, headers=headers)
		#get response
		con_resp = con.getresponse()
		body =		con_resp.read()
		headers =	dict(con_resp.headers)
		#close connection
		con.close()
	except Exception as e:
		body={}
		headers={}


	return (body,headers)
#
#main	###############################################
if __name__=='__main__':
	app.run(host='127.0.0.2',port=80)


'''
@app.route('/', methods=['GET','POST'])
def ():
	return endpoints.answer()
'''