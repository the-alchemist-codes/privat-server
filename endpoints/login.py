from .database import playerDB, unitDB, gearDB, itemDB, accesstokenDB

def login(request):
    pid = accesstokenDB[request.headers['Authorization']]['id']
    player = playerDB.find({'id':pid})

    if 'name' in player:
        body={
            'player':   {key:val for key,val in player.items() if key not in ['units','items','artifacts','parties','quests','trophyprogs','bingoprogs']},
            'units':    [unitDB.find({'iid':uid}) for uid in player['units']],
            'items':    [itemDB.find({'iid':iid}) for iid in player['items']],
            'artifacts': [gearDB.find({'iid':gid}) for gid in player['artifacts']] if 'artifacts' in player else [],
            'parties':  player['parties'],
            'cuid':     player['cuid'],
            'tut':      1,
        }
    else:
        body={}
    return body

def login_param(request):
    pid = accesstokenDB[request.headers['Authorization']]['id']
    player = playerDB.find({'id':pid})

    body = {
        'quests':       player['quests'],
        'trophyprogs':  player['trophyprogs'],
        'bingoprogs':   player['bingoprogs'],
        'channel':      1,
        'support':      0,
        'device_id':    player['device_id']
    }
    return body
